#! /bin/bash


# jj - projects starter
# Author: Felipe Freire
#
#

# ???????? 1st version V0.1
# ???????? version V0.2
# 20221125 version V0.3

### USE ###############################################
#
# 1. make sure a hidden folder lies in ~  ex: .projects
# 2. thats it, have fun !
#
### END ###############################################


projects_folder=~/.projects

version="jj - v0.3"

menu="	jj - manage your projects. 

		Usage	

	jj -c myproject	.. create a project	

	jj -d myproject .. delete a project

	jj -l		.. lists all projects

	jj -v		.. displays version

	jj -h		.. displays this menu
"

# test projects_folder variable exists or message 
[ "$projects_folder" ] || {
	echo "jj: no projects folder detected"
}


jj() {

	# check for at least one argument
	if [ "$#" -gt 0 ]; then

		case "$1" in

			# option that starts a project
			-c | --create )

				# check for at least 2 arguments
				if [ "$#" -gt 1 ]; then
	
					# filtering conflicting strings "-"
					if [[ "$2" != *"-"* ]]; then	

						# if directory does not exists ..
						if [[ ! -d "$projects_folder/$2" ]]; then
							# .. them create it
							mkdir "$projects_folder/$2"

						else
						# if project exists, echo:
						echo "jj: project already exists"	

						fi
					fi
				fi
 
				;;

			# option for deleting a project	
			-d | --del )

				# check for at least two arguments
				if [ "$#" -gt 1 ]; then

					# check if dir to be deleted does exist
					if [ -d "$projects_folder/$2" ]; then
						# then remove that directory 
						rm -rf "$projects_folder/$2"

					else
						# if directory does not exist:
						echo "jj: project does not exist"
					fi
				fi

				;;

			-l | --list ) 

				local c=0
				for d in $(ls $projects_folder); do
					echo "$c:	$d"
					c=$((c+1))
				done

				;;

			-v | --version )

				echo "$version"

				;;

			-h | --help )

				echo "$menu"

				;;

		esac
	else
		# if no arguments:
		echo "jj: no arguments to work with"
	fi
	
}
