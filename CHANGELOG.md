Changelog for jj


All notable change to this file project will be documented in this file.

version 0.1

. 1st version.
. simple program for creating project folders.
. added -c | --create option

version 0.2

. added command line options:

	. -d | --del		delete project
	. -v | --version	display version
	. -h | --help		display help menu
	. -l | --list		lists current projects

Version 0.3

 . option -l now enumerates projects:

  0:	project0
	1:	project1
	2:	project3
	...
